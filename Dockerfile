FROM alpine:edge

ARG PG_SOURCE_VERSION=12
ARG PG_TARGET_VERSION=15

RUN apk add --no-cache \
    postgresql${PG_SOURCE_VERSION} postgresql${PG_SOURCE_VERSION}-contrib \
    postgresql${PG_TARGET_VERSION} postgresql${PG_TARGET_VERSION}-contrib \
    su-exec

# https://github.com/docker-library/postgres/blob/cba2a05c03706daf5f9a66b93a447540b62df063/15/alpine3.18/Dockerfile#L164
RUN set -eux; \
    sed -ri "s!^#?(listen_addresses)\s*=\s*\S+.*!\1 = '*'!" /usr/share/postgresql/postgresql.conf.sample; \
    grep -F "listen_addresses = '*'" /usr/share/postgresql/postgresql.conf.sample

COPY overlay /

ENTRYPOINT ["/usr/local/bin/entrypoint"]
