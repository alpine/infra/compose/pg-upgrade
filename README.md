# pg-upgrade

This compose project can be used to migrate a postgres database to a newer major
version using `pg_upgrade`.

## Usage

Provide the following configuration in `.env`:

``` sh
PG_SOURCE_VERSION=12
PG_TARGET_VERSION=15
PG_USERNAME=user
```

The user is the database superuser.

Define the directories where the existing database exists and the upgraded
database will be placed. Create a `docker-compose.override.yml` file:

``` yaml
services:
  upgrade:
    volumes:
      - ./postgres${PG_SOURCE_VERSION}:/var/lib/postgresql/${PG_SOURCE_VERSION}/data
      - ./postgres${PG_TARGET_VERSION}:/var/lib/postgresql/${PG_TARGET_VERSION}/data
```

Adjust the host paths to the location where the databases are located (can also
be docker volumes).

This assumes the database is owned by `70:70` (`postgres:postgres` in Alpine
Linux), This has been tested against a database created with the
`postgres:12-alpine` image.

To execute the upgrade, run:

``` sh
docker compose run --build --rm upgrade
```

This will first initialize a new database for the target version, so the data
directory for the target version should be empty. Then it runs `pg_upgrade`.

To only run the compatibility check, you can provide the `--check` argument to
the container:

``` sh
docker compose run --build --rm upgrade --check
```
